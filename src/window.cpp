#include "window.hpp"

#include "qstandarditemmodel.h"
#include "ui_window.h"

#include <fstream>
#include <sys/socket.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>
#include <QGuiApplication>
#include <QStandardItemModel>

static pid_t pid_from_fd(int fd) {
    struct ucred ucred;
    socklen_t len = sizeof(struct ucred);
    if (getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &ucred, &len) == -1) {
        perror("getsockopt failed");
        exit(1);
    }
    return ucred.pid;
}

static std::string process_name_from_pid(const pid_t pid) {
    std::string procpath = std::format("/proc/{}/comm", pid);

    std::ifstream infile(procpath);
    if (infile.is_open()) {
        std::string out;
        std::getline(infile, out);
        infile.close();
        return out;
    } else {
        return "Unknown";
    }
}

static void registry_global(void* data, wl_registry* registry, uint32_t name, const char* interface, uint32_t version) {
    (void) registry;
    (void) name;

    if (std::string(interface).starts_with("wl_")) {
        return;
    }

    auto* window = static_cast<Window*>(data);
    window->addGlobal(interface, std::format("{}", version));
}

static void registry_global_remove(void* data, struct wl_registry* registry, uint32_t name) {
    (void) data;
    (void) registry;
    (void) name;
}

Window::Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent) : QMainWindow(parent), model(QStandardItemModel(0, 2)), ui(new Ui::Window) {
    ui->setupUi(this);
    auto* display = waylandApp->display();
    auto fd = wl_display_get_fd(display);
    auto pid = pid_from_fd(fd);
    auto pname = process_name_from_pid(pid);
    auto text = std::format("Compositor: {}", pname);
    ui->compositor->setText(QString::fromStdString(text));

    auto listener = wl_registry_listener{.global = registry_global, .global_remove = registry_global_remove};

    auto* registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &listener, (void*) this);
    wl_display_roundtrip(display);

    model.setHorizontalHeaderLabels({"Name", "Version"});
    ui->tableView->setModel(&model);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
}

Window::~Window() {
    delete ui;
}

void Window::addGlobal(const std::string interface, const std::string version) {
    auto items = QList<QStandardItem*>({
        new QStandardItem(QString::fromStdString(interface)),
        new QStandardItem(QString::fromStdString(version))
    });
    model.appendRow(items);
}
