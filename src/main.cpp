#include "window.hpp"

#include <QApplication>
#include <QLabel>

int main(int argc, char** argv) {
    auto app = QApplication(argc, argv);

    auto window = Window();
    window.show();

    return app.exec();
}
