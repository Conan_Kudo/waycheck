#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <QGuiApplication>
#include <QMainWindow>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
namespace Ui {
class Window;
}
QT_END_NAMESPACE

class Window : public QMainWindow {
    Q_OBJECT
    QStandardItemModel model;

  public:
    Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent = nullptr);
    ~Window();

    void addGlobal(const std::string interface, const std::string version);

  private:
    Ui::Window* ui;
};
#endif
